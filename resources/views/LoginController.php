<?php

namespace App\Http\Controllers\Auth;

use App\Models\employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{  
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    public function show_login_form()
    {
        return view('login');
    }

    public function process_login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->except(['_token']);

        $user = employee::where('email',$request->email)->first();

        if (auth()->attempt($credentials)) {

            return redirect()->route('employee');

        }else{
            session()->flash('message', 'Invalid credentials');
            return redirect()->back();
        }
    }

    public function logout()
    {
        \Auth::logout();

        return redirect()->route('login');
    }
}