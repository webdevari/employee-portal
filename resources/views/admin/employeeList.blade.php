@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
<table id="users" class="table table-bordered table-striped">
	<thead>
		<tr>
		  <th>Fullname</th>
		  <th>Email</th>
		  <th>Phone</th>
		  <th>Employee Code</th>
		  <th>Status</th>
		</tr>
	</thead>
	<tbody>
	    @foreach ($employees as $employee)
		    <tr>
	          <td><a href="employee/{{ $employee->id }}">{{ $employee->fullname }}</a></td>
	          <td>{{ $employee->email }}</td>
	          <td>{{ $employee->phone }}</td>
	          <td>{{ $employee->emp_code }}</td>
	          <td>{{ $employee->status }}</td>
	        </tr>
		@endforeach
	</tbody>
</table>
@stop

@section('css')
    <link href="{{ asset('assets/css/admin_custom.css') }}" rel="stylesheet">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop