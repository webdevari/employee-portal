<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'index'])->name('homepage');

Route::get('/test', [IndexController::class, 'test']);


// Route::get('/admin', [AdminController::class, 'index']);

Route::group(['middleware' => ['auth', 'admin']], function() {
    Route::get('/admin', [AdminController::class, 'index']);
});

Route::group(['middleware' => ['auth', 'employee']], function() {
    Route::get('/employee', [EmployeesController::class, 'index']);
});

Route::namespace('Auth')->group(function () {
  Route::get('/login',[LoginController::class, 'show_login_form'])->name('login');
  Route::post('/login',[LoginController::class, 'process_login'])->name('login');
  Route::get('/logout',[LoginController::class, 'logout'])->name('logout');
  // Route::get('/admin', [AdminController::class, 'index']);
  // Route::get('/employee', [EmployeesController::class, 'index']);
});

Route::get('redirects', [App\Http\Controllers\HomeController::class, 'index']);

// Route::get('/login', function () {
//     return view('auth/login');
// });

// Route::get('/register', function () {
//     return view('auth/register');
// });

// Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
