<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    //
    public function index() {
    	return view('auth/login');
    }

    public function test() {
    	return view('test');
    }
}
