<?php

namespace App\Http\Controllers\Auth;

use App\Models\employees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{  
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    public function show_login_form()
    {
        return view('login');
    }

    public function process_login(Request $request)
    {

        // dd($request);
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->except(['_token']);

        $user = employees::where('email',$request->email)->first();

        // dd($user);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'status' => $user->status])) {
            $request->session()->regenerate();
            // dd($user->role);
            if($user->role == 'admin')
                return redirect('/admin');
            else
                return redirect('/employee');
        }else{
            session()->flash('message', 'Invalid credentials');
            return redirect()->back();
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('logout');
    }
}