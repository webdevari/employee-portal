<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\employees;

class CreateEmployees extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'fullname' => 'Admin Test',
                'email' => 'admin@pkweb.in',
                'role' => 'admin',
                'password' => bcrypt('123456'),
                'age' => '40',
                'date_of_joining' => '2010-01-01',
                'emp_code' => 'WW0001',
                'phone' => '+917777778880',
                'department' => 'CEO',
                'blood_group' => 'B+',
                'image' => '',
                'status' => 'active'
            ], 
            [
                'fullname' => 'Employee Test',
                'email' => 'employee@pkweb.in',
                'role' => 'employee',
                'password' => bcrypt('Employee@weavers'),
                'age' => '30',
                'date_of_joining' => '2017-01-01',
                'emp_code' => 'WW0034',
                'phone' => '+91778889945450',
                'department' => 'Developer',
                'blood_group' => 'B+',
                'image' => '',
                'status' => 'active'
            ],
        ];

        foreach ($user as $key => $value) {
            employees::create($value);
        }
    }
}